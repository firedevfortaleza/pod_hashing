import pdb

class Usuario(object):
    def __init__(self, name, height):
        self.name = name
        self.height = height

class HashTable(object):

    def __init__(self, size):
        self.size = size
        self.slots = [[]] * size
        pass

    def put(self, key, value):
        indexLista = self.hash(key)
        listaI = self.slots[indexLista]
        indexItem = None
        for i in range (0, len(listaI)):
            if listaI[i].name == key:
                indexItem = i
                break

        if indexItem == None:
            listaI.append(value)
            return
        listaI[indexItem] = value
    
    def get(self, key):
        i = self.hash(key)
        return self.slots[i][0]
    
    def len(self):
        totalLength = 0
        for lista in self.slots:
            totalLength += len(lista)
        return totalLength
    
    def delete(self, key):
        return 0
    
    def printAll(self):
        for lista in self.slots:
            for item in lista:
                print(item)

    def hash(self, astring):
        sum = 0
        for pos in range(len(astring)):
            sum = sum + (ord(astring[pos]) * (pos + 1))

        return sum%self.size

if __name__ == "__main__":
    hashTable = HashTable(10)
    usuario = Usuario("teste", 1.84)
    usuario1 = Usuario("teste2", 1.94)
    hashTable.put(usuario.name, usuario)
    hashTable.printAll()


